import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { LoginComponent } from './login/login.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { AdminProductsComponent } from './admin-products/admin-products.component';
import { AdminOrdersComponent } from './admin-orders/admin-orders.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { Authguard } from './login/authguard.service';
import { adminAuthguard } from './login/admin-authguard.service';
import { ProductFormComponent } from './product-form/product-form.component';


const routes: Routes = [
  { path : '', component : HomeComponent },
  { path: 'products', component: ProductsComponent},
  { path: 'shopping-cart', component: ShoppingCartComponent},
  { path: 'login', component: LoginComponent},
  { path: 'order-success', component: OrderSuccessComponent, canActivate: [Authguard]},
  { path: 'admin/products', component: AdminProductsComponent, canActivate: [adminAuthguard]},
  { path: 'admin/orders', component: AdminOrdersComponent, canActivate: [adminAuthguard]},
  { path: 'my/orders', component: MyOrdersComponent, canActivate: [Authguard]},
  { path: 'admin/products/new', component: ProductFormComponent, canActivate: [Authguard, adminAuthguard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
