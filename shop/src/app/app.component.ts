import { Component } from '@angular/core';
import { AuthService } from './login/auth.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import 'firebase/auth';
import { UserService } from './login/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // title = 'shop';
  constructor(private auth: AuthService, private router: Router, private userService: UserService) {
   // console.log('AppComponent');
   // console.log(this.auth.user);
    this.authCheck();
  }

  authCheck() {
    firebase.auth().onAuthStateChanged(user => {
      if (!user) return;

      this.userService.save(user);

      let returnUrl = localStorage.getItem('returnUrl');
      if (!returnUrl) return;

      localStorage.removeItem('returnUrl');
      this.router.navigateByUrl(returnUrl);
    });
  }
}
