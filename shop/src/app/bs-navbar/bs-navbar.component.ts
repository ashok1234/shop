import { Component, OnInit } from '@angular/core';
import { AuthService } from '../login/auth.service';
import { AppUser } from '../models/app-user';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.scss']
})
export class BsNavbarComponent {
  constructor(public auth: AuthService) {}
  login() {
    this.auth.login();
  }

  logout() {
   this.auth.logout();
  }

}
