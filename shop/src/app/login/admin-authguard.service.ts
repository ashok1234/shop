import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class adminAuthguard implements CanActivate {

  constructor(private auth: AuthService, private  router: Router) {}

  canActivate() {
      return this.auth.appUser.isAdmin;
  }
}
