import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import 'firebase/auth';
import { AngularFireAuth } from  "@angular/fire/auth";
import {ActivatedRoute } from '@angular/router';
// import 'rxjs/add/observable/of';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
  appUser: any;
  constructor(private route: ActivatedRoute, private afAuth: AngularFireAuth, private userService: UserService) {
    this.authCheck();
  }

  authCheck() {
    firebase.auth().onAuthStateChanged(appUser => {
     if (appUser) {
      this.userService.get(appUser.uid).subscribe(user => {
        this.appUser = user;
      });
     } else {
       this.appUser = null;
     }
    });
  }

  login() {
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    localStorage.setItem('returnUrl', returnUrl);
    firebase.auth().signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    firebase.auth().signOut().then(x => {
      // Sign-out successful.
    }).catch((error) => {});
  }
}
