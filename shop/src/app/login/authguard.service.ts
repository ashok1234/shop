import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class Authguard implements CanActivate {

  constructor(private auth: AuthService, private  router: Router) {

  }
  canActivate(route, state: RouterStateSnapshot) {
    if (this.auth.appUser) {
      return true;
    }

    this.router.navigate(['/login'], {queryParams : {returnUrl: state.url}});
    return false;
  }
}
