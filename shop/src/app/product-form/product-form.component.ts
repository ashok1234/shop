import { Component, OnInit } from '@angular/core';
import { CategoryService } from './category.service';
import { ProductService } from '../products/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  categories$;

  constructor(private categoryService: CategoryService, private productService: ProductService) {
    this.categories$ = this.categoryService.getAll();
   }

  ngOnInit(): void {
  }

  save(product) {
    console.log('product');
    console.log(product);
    this.productService.create(product);
    // if (this.id) this.productService.update(this.id, product);
    // else this.productService.create(product);

    // this.router.navigate(['/admin/products']);
  }

}
