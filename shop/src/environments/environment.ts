// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyD36BUqIYitPUFElg44x2X4C85SVejEZcc",
    authDomain: "shop-889b1.firebaseapp.com",
    databaseURL: "https://shop-889b1.firebaseio.com",
    projectId: "shop-889b1",
    storageBucket: "shop-889b1.appspot.com",
    messagingSenderId: "568102233623",
    appId: "1:568102233623:web:d6fbcb1b0e0401ee9c5e52",
    measurementId: "G-XBPN45746L"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
